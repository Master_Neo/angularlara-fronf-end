(function() {
    var ZXCVBN_SRC = 'JS/node_modules/zxcvbn/dist/zxcvbn.js';

    var async_load = function() {
        var first, s;
        s = document.createElement('script');
        s.src = ZXCVBN_SRC;
        s.type = 'text/javascript';
        s.async = true;
        first = document.getElementsByTagName('script')[0];
        return first.parentNode.insertBefore(s, first);
    };

    if (window.attachEvent != null) {
        window.attachEvent('onload', async_load);
    } else {
        window.addEventListener('load', async_load, false);
    }
}).call(this);

(function() {

var app = angular
	.module("system_app",[
		"ngRoute",
		"ngResource"
		
		],
		function($interpolateProvider) {
		$interpolateProvider.startSymbol('[[');
		$interpolateProvider.endSymbol(']]');}
	).directive('restaurant', function(){  
   return{
		  restrict: 'E',
		  template: '<ul class="pagination">'+
			'<li ng-show="currentPage != 1"><a  class="btn btn-primary" href="javascript:void(0)" ng-click="getRestaurants(1)">&laquo;</a></li>'+
			'<li ng-show="currentPage != 1"><a  class="btn btn-primary" href="javascript:void(0)" ng-click="getRestaurants(currentPage-1)">&lsaquo; Prev</a></li>'+
			'<li ng-repeat="i in range">'+
			'<a class="btn btn-primary" href="javascript:void(0)" ng-click="getRestaurants(i)">[[i.name]]</a></a>'+
			'</li>'+
			'<li ng-show="currentPage != totalPages"><a class="btn btn-primary" href="javascript:void(0)" ng-click="getRestaurants(currentPage+1)">Next  &rsaquo;</a></li>'+
			'<li ng-show="currentPage != totalPages"><a class="btn btn-primary" href="javascript:void(0)" ng-click="getRestaurants(totalPages)">&raquo;</a></li>'+
		  '</ul>'
		};
	})
	.config(['$routeProvider', function($routeProvider)
	{
		
		$routeProvider
		.when('/home',{
			templateUrl: 'HTML/templates/home.html',
			controller:'ListCtrl'
		})
		.when('/list',{
			templateUrl: 'HTML/templates/list.html',
			controller:'ListCtrl'
		})
		.when('/editAdmin/:id',{
			templateUrl: 'templates/editAdmin.html',
			controller:'EditAdminCtrl'
		})
		.when('/editRestuarant/:id',{
			templateUrl: 'HTML/templates/form.html',
			controller:'EditRestaurantCtrl'
		})
		.when('/viewRestaurant/:id',{
			templateUrl: 'HTML/templates/viewRestaurant.html',
			controller:'viewRestaurantCtrl'
		})
		.when('/viewUser/:id',{
			templateUrl: 'templates/viewAuthUser.html',
			controller:'viewAdminCtrl'
		})
		.when('/createRestaurant',{
			templateUrl: 'HTML/forms/addRestaurant.html',
			controller:'CreateRestaurantCtrl'
		})
		.when('/signIn',{
			templateUrl: 'HTML/templates/login.html',
			controller:'SignInAdminCtrl'
		})
		.when('/signUp',{
			templateUrl: 'HTML/templates/signUp.html',
			controller:'SignUpAdminCtrl'
		})
		.when('/map',{
			templateUrl: 'HTML/templates/home.html',
			controller:'MyMapCtrl'
		})	
		
		.otherwise({redirectTo:'/home'});
	}])
//login

	.controller('SignInAdminCtrl',['$scope','RestaurantAdministrator','$window',function($scope,RestaurantAdministrator,$window){
		$scope.loginlinkTextSettings = {
			pageTitle:"Sign In Admin",
			action:"Login",
			linkText:''
		};
		$scope.viewMapLinkTextSettings = {
			pageTitle:"Sign In Admin",
			action:"Login",
			linkText:'View Map'
		};
		$scope.settings = {
		};
		
		$scope.admin = {
			email:'',
			password:''
		};

		$scope.submit = function()
		{	
			 //validate empty fields 
		   		
		   
		   RestaurantAdministrator.update($scope.admin).$promise.then(function(data,$window)
		   {
					if(data.msg)
					{	
						$scope.username = data.username;
						window.location.href = '#!/list';
					//	window.localStorage.setItem('username',data.username.id);
					//	window.localStorage.setItem('userId') = data.user_id.id;
					}
				}).catch(function(error){
					return error;
				});
			}		
	}])
	//register 
	.controller('SignUpAdminCtrl',['$scope','RestaurantAdministrator',function($scope,RestaurantAdministrator){
		
		
		$scope.settings = {
			pageTitle:"Sign Up Admin",
			action:"Create"
		}
		$scope.restaurantAdministrator = {
			fullname:'',
			tel:'',
			username:'',
			address:'',
			email:'',
			password:''
		};
		$scope.submit = function(){		
			
			RestaurantAdministrator.save($scope.restaurantAdministrator).$promise.then(function(data)
			{
				if(data.msg)
				{
					angular.copy({},$scope.restaurantAdministrator);
					$scope.settings.success = "Admin created!!";
					console.log("Okay : " + data.msg);
				}
			}).catch(function(error){
				console.log("Error : " +error);
				return error;
			});
	};
	}])
	//view auth data
	
	.controller('EditAdminCtrl',['$scope','RestaurantAdministrator','$routeParams',function($scope,RestaurantAdministrator,$routeParams){
		$scope.settings = {
			pageTitle:"Edit User",
			action:"Edit"
		}
		var id = $routeParams.id;
		RestaurantAdministrator.get({id:id}, function(data)
			{
				$scope.restaurantAdministrator = data.restaurantAdministrator;
				
			});
			
			$scope.submit = function()
			{
			
					RestaurantAdministrator.update({id:$scope.restaurantAdministrator.id}, $scope.restaurantAdministrator).$promise.then(function(data)
						{
							if(data.msg)
							{
								$scope.settings.success = "User Edited!!";
							}
						}).catch(function(error){
						console.log("Error Editing user data");
						return error;
					})
			};
	}])
	.controller('AuthCtrl',['$scope','RestaurantAdministrator','$route','$routeParams',function($scope,RestaurantAdministrator,$route,$routeParams){
		// Auth User 
		var id = $routeParams.id;
		RestaurantAdministrator.get({id:id}, function(data)
			{
				$scope.restaurantAdministrator = data.restaurantAdministrator;			
			});
	}])
	.controller('HomeCtrl',['$scope','Restaurant','$route','$routeParams',function($scope,Restaurant,$route,$routeParams){
		
		
		
		
		
		
		
	}])/*
	app.service('Map', function($q) {
    
    this.initMap = function() {
		
        var options = {
            center: new google.maps.LatLng(-30.5595, 22.9375),
            zoom: 4,
            disableDefaultUI: true    
        }
        this.map = new google.maps.Map(
            document.getElementById("map"), options
        );
		
		this.addMarker = function () {
			
			this.marker = new google.maps.Marker({
				map: this.map,
				position: {lat:-30.5595,lng:22.9375},
				animation: google.maps.Animation.DROP
			});	
		};
		
    };
	})*/.controller('ListCtrl',['$scope'/*,'Map'*/,'Restaurant','RestaurantAdministrator','$route','$routeParams',function($scope,/*Map,*/Restaurant,RestaurantAdministrator,$route,$routeParams){
		
		//localStorage.
		$scope.authUser = {
		}
		//$scope.authUser.username = window.localStorage.getItem('username');	
	
		// all clients 
		  $scope.restaurants = [];
		  $scope.totalPages = 0;
		  $scope.currentPage = 1;
		  $scope.range = [];
		  $scope.getRestaurants = function(pageNumber){

			if(pageNumber===undefined){
			  pageNumber = '1';
			}
			//send request( page number to show )
			Restaurant.get({page: pageNumber},function(response){
			// Old pagination style using http
			// $http.get('/posts-json?page='+pageNumber).success(function(response) { 

			  $scope.restaurants  = response.restaurants.data;
			  $scope.totalPages   = response.restaurants.last_page;
			  $scope.currentPage  = response.restaurants.current_page;
				
			  // Pagination Range
			  var pages = [];

			  for(var i=1;i<=response.restaurants.last_page;i++) {          
				pages.push(i);
			  }
			  $scope.range = $scope.restaurants; 
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		  
	$scope.addMarker = function(restaurants){
	
		var mapOptions = {
			zoom: 4,
			center: new google.maps.LatLng(-30.5595,22.9375),
			mapTypeId: google.maps.MapTypeId.TERRAIN
    }

		$scope.map = new google.maps.Map(document.getElementById('map'), mapOptions);    
		var infoWindow = new google.maps.InfoWindow();    
        
		var marker = new google.maps.Marker({
            map: $scope.map,
            position: new google.maps.LatLng(restaurants.latitude, restaurants.longitude)
        });
        marker.content = '<div class="infoWindowContent">' + restaurants.name + '</div>';
        
        google.maps.event.addListener(marker, 'click', function(){
            infoWindow.setContent('<h2></h2>' + marker.content);
            infoWindow.open($scope.map, marker);
        });
	console.log(" Restaurant's Name : " + restaurants.name);
    
	
			  
			
			};
			
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			$scope.getDirections = function(latitude,longitude){
			/*
			var directionDisplay = new google.maps.DirectionsRenderer();			
			var directionService = new google.maps.DirectionsService();
			
			var map;
			
			var pointA = google.maps.LatLng(-30.5595,22.9375/*restaurant.latitude,restaurant.longitude);
			var pointB = google.maps.LatLng($scope.latitude,$scope.longitude);
			var mapOptions = {
			zoom: 4,
			center: new google.maps.LatLng(-30.5595,22.9375),
			mapTypeId: google.maps.MapTypeId.TERRAIN
				
				
			};
			map = new google.maps.Map(document.getElementById('map'), mapOptions);    
			directionDisplay.setMap(map);
			
			function calculateRoute(){
				var request = {
					origin:pointA,
					destination:pointB,
					travelMode:'DRIVING'
					
				};
				
				directionService.route(request, function(result,status){
				console.log(result, status);
				if(status === 'OK'){
					directionDisplay.setDirections(result);
				}
				});
			}
			
			document.getElementById('get').onclick = function(){
				calculateRoute();
			}*/
			}
		
		//Auth User to storage for display in home page (to display auth user)
		//$scope.Auth_user = window.localStorage.getItem('Auth_user');
		
		//logout 
		
		  
		  $scope.logout = function()
		{		
		//use localStorage	instead of api	
		   /*RestaurantAdministrator.get().$promise.then(function(data,$window)
		   {
					if(data)
					{
										
					}
				}).catch(function(error){
					return error;
				});
			}*/
		
		
		  }
		  //remove restuarant 
		$scope.destroy = function(id){
		Restaurant.delete({id:id}).$promise.then(function(data)
			{
				$scope.msg = "Deleted!";
				if(data.msg)
				{
					
					$route.reload();
				}
				
			})
			.catch(function(error){
					return error;
				});
			}
		}); 
		}
		//Map.initMap();
	}])
.controller('viewRestaurantCtrl',['$scope','Restaurant','$route','$routeParams',function($scope,Restaurant,$route,$routeParams){	
	//view client 
		var id = $routeParams.id;
		Restaurant.get({id:id}, function(data)
			{	
					$scope.restaurant = data.restaurant;
			});
	}])
	.controller('EditRestaurantCtrl',['$scope','Restaurant','$routeParams',function($scope,Restaurant,$routeParams){
		$scope.settings = {
			pageTitle:"Edit User",
			action:"Edit"
		}
		var id = $routeParams.id;
		Restaurant.get({id:id}, function(data)
			{
				$scope.restaurant = data.restaurant;
				
			});
			
			$scope.res = {
				name:'',
				tel:'',
				address:'',
				email:'',
				latitude:'',
				longitude:''
			}
			
			$scope.submit = function()
			{
				Restaurant.update({id:$scope.restaurant.id}, {
				name:$scope.restaurant.name,
				tel:$scope.restaurant.tel,
				address:$scope.restaurant.address,
				email:$scope.restaurant.email,
				latitude:$scope.restaurant.latitude,
				longitude:$scope.restaurant.longitude
				}).$promise.then(function(data)
						{
							if(data.msg)
							{
								$scope.settings.success = "Restaurant Edited!!";//custome obj
							}
							
						}).catch(function(error){//catch error o avoid errors such as, unhandled rejection,
						return error;
					});
			}	
	}])
	//create restaurant, inject, scope, Lara angular resouce, add custom setting to display alert and objects attr
	.controller('CreateRestaurantCtrl',['$scope','Restaurant',function($scope,Restaurant){
		
		$scope.settings = {
			pageTitle:"Sign Up Restaurant",
			action:"Create"
		}
		$scope.restaurant = {
			name:'',
			tel:'',
			address:'',
			email:'',			
			latitude:'',
			latitude:'',
			user_id:'1'
		};
		
		$scope.submit = function()
		{			   
				Restaurant.save($scope.restaurant).$promise.then(function(data)
				{
					if(data.msg)
					{
						angular.copy({},$scope.admin);
						$scope.settings.success = "Restaurant created!!";
					}
				}).catch(function(error){
					return error;
				});
		};
	}])
	.controller('MyMapCtrl',['$scope','Restaurant', function($scope,Restaurant) {
	$scope.m = [];
	Restaurant.get(function(response){
		
	$scope.markers = response.markers;	
	//$scope.m.push({coords:{lat:response.markers.latitude,lng:response.markers.longitude}});
	
	
	 
	 
	$scope.addMarker = function(markers){		
	
	 console.log(markers.latitude);
	
		var mapOptions = {
			zoom: 4,
			center: new google.maps.LatLng(-30.5595,22.9375),
			mapTypeId: google.maps.MapTypeId.TERRAIN
    }

			    
			
			for(var i=0; i < $scope.markers.length ; i++){			
				
				
				$scope.map = new google.maps.Map(document.getElementById('map'), mapOptions);    
				var infoWindow = new google.maps.InfoWindow();
				
				var marker = new google.maps.Marker({
					map: $scope.map,
					position: new google.maps.LatLng(markers.latitude, markers.longitude)
				});
				marker.content = '<div class="infoWindowContent">' + markers.name + '</div>';
				
				google.maps.event.addListener(marker, 'click', function(){
					infoWindow.setContent('<h2></h2>' + marker.content);
					infoWindow.open($scope.map, marker);
				});
				}
				
			};
			
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			$scope.getDirections = function(latitude,longitude){
			/*
			var directionDisplay = new google.maps.DirectionsRenderer();			
			var directionService = new google.maps.DirectionsService();
			
			var map;
			
			var pointA = google.maps.LatLng(-30.5595,22.9375/*restaurant.latitude,restaurant.longitude);
			var pointB = google.maps.LatLng($scope.latitude,$scope.longitude);
			var mapOptions = {
			zoom: 4,
			center: new google.maps.LatLng(-30.5595,22.9375),
			mapTypeId: google.maps.MapTypeId.TERRAIN
				
				
			};
			map = new google.maps.Map(document.getElementById('map'), mapOptions);    
			directionDisplay.setMap(map);
			
			function calculateRoute(){
				var request = {
					origin:pointA,
					destination:pointB,
					travelMode:'DRIVING'
					
				};
				
				directionService.route(request, function(result,status){
				console.log(result, status);
				if(status === 'OK'){
					directionDisplay.setDirections(result);
				}
				});
			}
			
			document.getElementById('get').onclick = function(){
				calculateRoute();
			}*/
		}	
		});
	}])
	app.factory('RestaurantAdministrator',function($resource){
			return $resource("http://localhost/RestaurantAdvisor/public/Users/:id",{id:"@_id"},{
			update:{method:"PUT", params: {id:"@id"}}	
			})	
	})
	app.factory('Restaurant',function($resource){
			return $resource("http://localhost/RestaurantAdvisor/public/Restaurant/:id",{id:"@_id"},{
			update:{method:"PUT", params: {id:"@id"}}	
			})	
	})/*
	app.service('Map', function($q) {
    
    this.initMap = function() {
		
        var options = {
            center: new google.maps.LatLng(30.5595, -22.9375),
            zoom: 4,
            disableDefaultUI: true    
        }
        this.map = new google.maps.Map(
            document.getElementById("map"), options
        );
        this.restaurants = new google.maps.places.PlacesService(this.map);
    }
    
    this.search = function(str) {
        var d = $q.defer();
        this.places.textSearch({query: str}, function(results, status) {
            if (status == 'OK') {
                d.resolve(results[0]);
            }
            else d.reject(status);
        });
        return d.promise;
    }
    
    this.addMarker = function(res) {
        if(this.marker) this.marker.setMap(null);
        this.marker = new google.maps.Marker({
            map: this.map,
            position: res.geometry.location,
            animation: google.maps.Animation.DROP
        });
        this.map.setCenter(res.geometry.location);
    }
    
})*/
.controller('FormController', function($scope) {})		
//count length
        .filter('passwordCount', [function() {
            return function(value, peak) {
                value = angular.isString(value) ? value : '';
                peak = isFinite(peak) ? peak : 7;

                return value && (value.length > peak ? peak + '+' : value.length);
            };
        }])
		//you can find this api online 
        .factory('zxcvbn', [function() {
            return {
                score: function() {
                    var compute = zxcvbn.apply(null, arguments);
                    return compute && compute.score;
                }
            };
        }])

        .directive('okPassword', ['zxcvbn', function(zxcvbn) {
            return {
                // restrict to only attribute and class
                restrict: 'AC',

                // use the NgModelController
                require: 'ngModel',

                // add the NgModelController as a dependency to your link function
                link: function($scope, $element, $attrs, ngModelCtrl) {
                    $element.on('blur change keydown', function(evt) {
                        $scope.$evalAsync(function($scope) {
                            // update the $scope.password with the element's value
                            var pwd = $scope.password = $element.val();

                            // resolve password strength score using zxcvbn service
                            $scope.passwordStrength = pwd ? (pwd.length > 7 && zxcvbn.score(pwd) || 0) : null;
                            
                            // define the validity criterion for okPassword constraint
                            ngModelCtrl.$setValidity('okPassword', $scope.passwordStrength >= 2);
                        });
                    });
                }
            };
        }]);
		
		
		
		
		///////////////////////////////


})();




								